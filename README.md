# enpitsu-cli 鉛筆

enpitsu-cli (https://gitlab.com/japorized/enpitsu) rewritten in Rust

**Note**: May be in conflict with enpitsu-js, which itself may or may not be rewritten

---

## Current Status

**Not usable.**

The program is still in its infancy (although it has been around for a while!),
where ideas are being properly ironed out as I figure out how I want to build enpitsu.

Follow my dev notes on [my blog](https://japorized.ink/tags/enpitsu).

## Development

```
git clone https://gitlab.com/enpitsu/enpitsu-cli
cd enpitsu-cli
cargo run
```

## Current Usage

No functionalities right now. Most functions are just shells, and you will probably
get an error when no `task.json` file is found in `$XDG_DATA_HOME/enpitsu`.
Copy `test/task.json` over to that directory to run the program if you want to play
with it.

```
cargo run
```

---

## TODO

1. rewrite taskmaster and task to make use of enpitsu-lib
2. write unit tests when item 1 is done.

---

## Current Goals (volatile information)

The goal of enpitsu has changed from being a simple-to-make program, to one
where I focus on component modularity. This is in hopes of allowing different components
of the stack to be used by other programs/clients.

For instance, right now, `enpitsu-lib` is a Rust-only api meant for writing clients
(cli, tui, etc) in Rust. `enpitsu-api` is meant to be a RESTful API that uses `enpitsu-lib`
as its dependency. `enpitsu-cli`, which is this project, is a on-your-local-machine cli
application to depends on `enpitsu-lib`.

The current goals of each project is as such:
* `enpitsu-lib` is meant to be a very simple library that interfaces with a SQLite database file.
  It should be so simple that it is easily rewritable in another language.
* `enpitsu-api` is an API server, opening up the door to having `enpitsu` as a server,
  allowing any sort of remote client to access their database.
* `enpitsu-cli` is a CLI client for enpitsu written in Rust, that aims to be feature-comparable
  with the original JS version.
  Ideally, it will not only pull `enpitsu-lib` as a dependency, but also allow for interaction with
  a remote `enpitsu-api`.

The "overarching goal" of the whole project is to simply provide me with a place to learn and sharpen
my programming skills, software agility, architectural know-hows, etc etc, a bunch of seemingly
grandiose goals that are just nonsense. (Read: I just want to have fun building and learning things.)
