# About the tool

## Task

A task has the following fields:

```
pub struct Task {
    pub id: usize,
    pub in_use: bool,
    pub title: String,
    pub start: DateTime<Local>,
    pub end: DateTime<Local>,
    pub tags: Vec<String>,
    pub priority: u8,
    pub notes: String,
    pub status: String,
    pub star: bool,
    creation: DateTime<Local>
}
```

### About each field

* fields marked with [required] are fields that must be initialized by the user

#### `id`

* unique identifier of a Task

#### `in_use`

* shows that a Task is in use (can and should be deprecated)

#### `title`

* [required]
* title of the task

#### `start`

* [required]
* if the task has a duration, start is the starting date
* if the task has no duration, start is the fixed dummy date:
```
const DEFAULT_DATETIME_STR: &str = "1995-01-01T05:00:00.000Z";
```

#### `end`

* [required]
* if the task has a duration, end is the ending date
* if the task has no duration, end is the fixed dummy date:
```
const DEFAULT_DATETIME_STR: &str = "1995-01-01T05:00:00.000Z";
```

#### `tags`

* list of tags for this task
* we want to search through this later on so a Vector may not be the most appropriate
* default: []

#### `priority`

* priority of the task; has meaning along the lines of:
  * 0: no priority
  * 1: important and urgent
  * 2: urgent
  * 3: important
* default: 0

#### `notes`

* comments for this task

#### `status`

* status of the task
  * possible values: 'active', 'completed'
* default: 'active'

#### `star`

* whether the task is starred or not
* default: false

#### `creation`

* creation date of the task
* default: literally the creation date

---

# Functionalities

## Add

Users should have the option to either provide all arguments, partial arguments, or no arguments at all at the cli.

Bonus problem: User can provide a json file similar to tasks.json sans id, creation and status, and the program should add every listed task as a new Task.

Program should figure out missing values and ask the user for additional input on missing values, if any.

### Arguments

```
-T, --title       Title of the task
-s, --start       Start date of the task
-e, --end         End date of the task
-t, --tags        Tags for the task
-p, --priority    Priority for the task (options: 0, 1, 2, 3)
-n, --notes       Note for the task
-S, --star        Star the task
```

### Examples

#### Adding new task without arguments

```
$ enpitsu add
Title: 
```

#### Adding new task with partial arguments

```
$ enpitsu add -T "New task" -t "personal,study"
Creating task "New Task"
Start date: |
```

Once end date is given...

```
$ enpitsu add -T "New task" -s "2019 Jan 20, 15:00" -e "2019 Feb 10, 12:00" -t "personal,study"
Creating task "New Task"
Start date: 2019 Jan 20, 15:00
End date: |
```

**Note**:
* if user hits enter on Start date without giving any values, default to now
* if user types `none` for Start date, set start to dummy date
  * `none` here would mean that the task is not meant to have a duration
  * in this case, the program will also skip asking about the End date
* End date should either take a date, or empty to mean 24 hours in the future

#### Adding new task with all arguments

Example 1: all required fields are given
```
$ enpitsu add -T "New task" -s "2019 Jan 20, 15:00" -e "2019 Feb 10, 12:00"
New task "New task" created
 2          New task
 Duration: 2019 Jan 20, 15:00:00 to 2019 Feb 10, 12:00:00
 Tags: 
 Status: active
 Notes:
```

Example 2: more than just required fields are given
```
$ enpitsu add -T "New task" -s "2019 Jan 20, 15:00" -e "2019 Feb 10, 12:00" -p 2 -t "personal,study" -n "Review this task" -S
New task "New task" created
 2 *  (!! ) New task
 Duration: 2019 Jan 20, 15:00:00 to 2019 Feb 10, 12:00:00
 Tags: personal, study
 Status: active
 Notes: Review this task
```

#### Adding new task(s) by file

```
$ enpitsu add -f newtasks.json
```

If any of the tasks has missing info that is required, query the user like in the case of partial arguments.
