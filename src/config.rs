extern crate dirs;

use std::{fs, path};
use crate::utils;

#[derive(Debug)]
pub struct Config {
    pub config_file: path::PathBuf,
    pub task_file: path::PathBuf,
    pub editor: path::PathBuf
}

impl Config {
    // Defaults:
    // config_file: $XDG_CONFIG_HOME/enpitsu/config.json
    // task_file: $XDG_DATA_HOME/enpitsu/tasks.db
    // editor: /usr/bin/nano
    pub fn new(appname: &str) -> Config {
        // set the defaults
        let mut config_file = dirs::config_dir()
            .expect("config_file should point to a directory");
        config_file.push(appname);
        config_file.push("config.json");

        let mut task_file = dirs::data_dir()
            .expect("task_location should point to a directory");
        task_file.push(appname);
        task_file.push("tasks.json");

        let mut editor = path::PathBuf::from("/usr/bin/nano");

        // read values from config.json and get them out
        let config_path_str = config_file.to_str()
            .expect("config.json failed to resolve");
        match utils::read_file_to_json(config_path_str) {
            Ok(result) => {
                let null = String::from("null");

                let task_location = result["taskLocation"].to_string();
                if task_location != null {
                    task_file = path::Path::new(&task_location).to_path_buf();
                }

                editor = path::PathBuf::from(result["editor"].to_string());
            },
            Err(error) => {
                println!("{:?}", error);
                match fs::File::create(&config_path_str) {
                    Ok(f) => {
                        println!("Created file: {:?}", f);
                        ()
                    },
                    Err(err) => {
                        println!("{:?}", err);
                        ()
                    }
                };
            }
        }

        Config {
            config_file,
            task_file,
            editor
        }
    }
}
