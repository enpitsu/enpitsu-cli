extern crate chrono;
extern crate colorful;
extern crate json;

use chrono::prelude::*;
use colorful::{Color, Colorful};


/// A generic type for a Task
#[derive(Debug)]
pub struct Task {
    pub id: usize,
    pub in_use: bool,
    pub title: String,
    pub start: DateTime<Local>,
    pub end: DateTime<Local>,
    pub tags: Vec<String>,
    pub priority: u8,
    pub notes: String,
    pub status: String,
    pub star: bool,
    creation: DateTime<Local>
}

fn json_value_to_str(jvalue: &json::JsonValue) -> &str {
    jvalue.as_str().expect("Could not take &str from JsonValue")
}

impl Task {
    pub fn new(json_task: &json::JsonValue) -> Task {
        const DEFAULT_DATETIME_STR: &str = "1995-01-01T05:00:00.000Z";

        let parse_time_to_date_time = 
            | time: &str | time.parse::<DateTime<Local>>()
            .expect("Could not parse time, string should be of format DateTime<Local>");

        let start: DateTime<Local>;
        let end: DateTime<Local>;
        let json_start: &str = json_value_to_str(&json_task["start"]);
        if json_start == "none" ||
           json_start == "" {
            start = parse_time_to_date_time(DEFAULT_DATETIME_STR);
            end = parse_time_to_date_time(DEFAULT_DATETIME_STR);
        } else {
            start = parse_time_to_date_time(json_start);
            end = parse_time_to_date_time(json_value_to_str(&json_task["end"]));
        }

        let mut tags: Vec<String> = Vec::new();
        for i in 0..json_task["tags"].len() {
            tags.push(json_task["tags"][i].to_string());
        }

        let creation: DateTime<Local>;
        if json_value_to_str(&json_task["creation"]) != "" {
            creation = parse_time_to_date_time(json_value_to_str(&json_task["creation"]));
        } else {
            creation = parse_time_to_date_time(DEFAULT_DATETIME_STR);
        }

        Task {
            id: json_task["id"].as_usize()
                .expect("id should be a Number"),
            in_use: json_task["inUse"].as_bool()
                .expect("inUse should be a Boolean"),
            title: json_task["title"].to_string(),
            start: start,
            end: end,
            tags: tags,
            priority: json_task["priority"].as_u8()
                .expect("Should be a number"),
            notes: json_task["notes"].to_string(),
            status: json_task["status"].to_string(),
            star: json_task["star"].as_bool()
                .expect("star should be a Boolean"),
            creation: creation
        }
    }

    pub fn print_desc(&self) {
        let star = if self.star { "*" } else { " " };
        let priority = match self.priority {
            1 => "(!!!)",
            2 => "(!! )",
            3 => "(!  )",
            _ => "     "
        };

        println!(" {} {} {} {}",
                 self.id.to_string().color(Color::Grey37),
                 star.color(Color::Yellow),
                 priority.color(Color::Red),
                 self.title);
    }

    pub fn print_full_desc(&self) {
        let tags_str = self.get_tags_as_string();

        self.print_desc();
        println!(
            " {}  : {} to {}",
            "Duration".color(Color::Magenta),
            self.start.to_string(),
            self.end.to_string()
        );
        println!(" {}      : {}", "Tags".color(Color::Magenta), tags_str);
        println!(" {}    : {}", "Status".color(Color::Magenta), self.status);
        if self.notes != "" {
            println!(" {}     :\n{}", "Notes".color(Color::Magenta), self.notes);
        }
        println!("");
    }

    fn get_tags_as_string(&self) -> String {
        let mut tags_str: String = "".to_string();
        for i in 0..self.tags.len() {
            tags_str.push_str(&self.tags[i].to_string());
            if i != self.tags.len() - 1 {
                tags_str.push_str(&", ".to_string());
            }
        }

        tags_str
    }
}

impl Clone for Task {
    fn clone(&self) -> Task {
        Task {
            id       : self.id,
            in_use   : self.in_use,
            title    : self.title.clone(),
            start    : self.start,
            end      : self.end,
            tags     : self.tags.clone(),
            priority : self.priority,
            notes    : self.notes.clone(),
            status   : self.status.clone(),
            star     : self.star,
            creation : self.creation
        }
    }
}
