// requires revision as we change to using enpitsu-lib

extern crate colorful;
extern crate json;

mod task;

use colorful::{Color,Colorful};
use self::task::Task;
use std::process;

#[derive(Debug)]
pub struct TaskMaster {
    active: u64,
    completed: u64,
    starred: u64,
    total: u64,
    tasks: Vec<Task>
}

impl TaskMaster {
    pub fn new(parsed: &json::JsonValue) -> TaskMaster {
        // index 0 of the file should always contain
        // summary regarding the tasks
        let summary = &parsed["0"];

        // let the taskmaster understand what tasks are under it
        let mut tasks: Vec<Task> = Vec::new();
        let mut task: Task;
        for i in 1..parsed.len() {
            task = Task::new(&parsed[i.to_string()]);
            if task.in_use {
                tasks.push(task);
            }
        }

        TaskMaster {
            active: summary["active"].as_u64()
                .expect("active should be a Number"),
            completed: summary["completed"].as_u64()
                .expect("completed should be a Number"),
            starred: summary["starred"].as_u64()
                .expect("starred should be a Number"),
            total: summary["total"].as_u64()
                .expect("total should be a Number"),
            tasks: tasks
        }
    }

    pub fn move_task(mut self, target: usize, dest: usize) {
        let targetkey_result = self.tasks.binary_search_by_key(&target, |t| t.id);
        let targetkey: usize;
        match targetkey_result {
            Ok(n) => targetkey = n,
            Err(_e) => {
                println!("Task with index {} not found.", target);
                process::exit(1);
            }
        }
        let destkey_result = self.tasks.binary_search_by_key(&dest, |t| t.id);
        let destkey = match destkey_result {
            Ok(n) => {
                if self.tasks[n].in_use {
                    self.tasks[n].print_full_desc();
                    // ask if user wants to replace this task with target task
                }

                n
            },
            Err(_e) => {
                println!("Task with index {} not found.", dest);
                println!("Assigning id of {} directly to task {}", dest, target);

                dest
            },
        };

        self.tasks[targetkey].id = dest;
        self.tasks[targetkey].print_full_desc();
        match destkey_result {
            Ok(_n) => {
                self.tasks.remove(destkey);
            },
            Err(_e) => { },
        }
        self.tasks.sort_by_key(|t| t.id);

        dbg!{&self.tasks};
    }

    // pub fn remove_empty_tasks(&mut self) {
    //     for i in (0..self.tasks.len()).rev() {
    //         if ! self.tasks[i].in_use {
    //             &self.tasks.remove(i);
    //         }
    //     }
    // }

    pub fn remove_tasks(&self, s: String) { 
        let targets = utils::multitargets(s);
        let mut target_keys: Vec<usize> = Vec::new();
        for target in targets {
            match self.tasks.binary_search_by_key(&target, |t| t.id) {
                Ok(n) => target_keys.push(n),
                Err(_e) => {
                    println!("Target task with id {} not found. Ignoring...", target);
                }
            }
        }

        if target_keys.len() > 0 {
            // show tasks
            for key in target_keys {
                self.tasks[key].print_full_desc();
            }
            println!("Are you sure you want to remove these tasks?");
            // get confirmation
            //  Ok => remove tasks
            //  No => stop
        }
    }

    pub fn show_summary(&self) {
        println!(
            "\n   {}: {}   {}: {}   {}: {}",
            "Active".color(Color::Green), self.active.to_string(),
            "Completed".color(Color::Cyan), self.completed.to_string(),
            "Starred".color(Color::Yellow), self.starred.to_string()
        );
    }

    pub fn show_active_tasks(&self) {
        for task in &self.tasks {
            if task.status == "active" {
                task.print_desc();
            }
        }
        self.show_summary();
    }

    pub fn show_active_tasks_in_full(&self) {
        for task in &self.tasks { 
            if task.status == "active" {
                task.print_full_desc();
            }
        }
        self.show_summary();
    }
}

mod utils {
    pub fn multitargets(s: String) -> Vec<usize> {
        let raw_targets: Vec<&str> = s.split(',').collect();
        let mut targets: Vec<usize> = Vec::new();
        for raw_target in raw_targets {
            match raw_target.find('-') {
                Some(_n) => {
                    targets.append(&mut range_to_vec(raw_target));
                },
                None => {
                    targets.push(
                        raw_target.parse::<usize>().expect("Should be an integer")
                    );
                }
            }
        }

        targets
    }

    pub fn range_to_vec(s: &str) -> Vec<usize> {
        let startend_str: Vec<&str> = s.split('-').collect();
        let mut startend: Vec<usize> = Vec::with_capacity(2);
        startend[0] = startend_str[0].parse::<usize>().expect("Should be an integer");
        startend[1] = startend_str[1].parse::<usize>().expect("Should be an integer");
        let mut values: Vec<usize> = Vec::new();
        for i in startend[0]..startend[1] {
            values.push(i);
        }

        values
    }
}

