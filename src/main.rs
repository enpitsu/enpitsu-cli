extern crate structopt;

pub mod config;
pub mod utils;
pub mod cli_args;

use {
    cli_args::{ CliArgs, Action, CfgAction },
    structopt::StructOpt,
    enpitsu::{
        taskmaster::TaskMaster
    }
};

fn main() {
    let cli_args: CliArgs = CliArgs::from_args();
    println!("{:?}", cli_args);

    let appname = "enpitsu";
    let config = config::Config::new(appname);
    println!("{:?}", config);

    // to be replaced with using enpitsu-lib
    let parsed = match utils::read_file_to_json(config.task_file.to_str().unwrap()) {
        Ok(j) => j,
        Err(error) => panic!("Failed to parse task json file: {:?}", error),
    };
    let tm = TaskMaster::new(&parsed);

    match &cli_args.action {
        Action::Add { ref title } => {
            println!("Add task called: {}", title);
        },

        Action::Config { cfgaction } => {
            match cfgaction {
                CfgAction::Get { database, editor } => {
                    println!("database: {}, editor: {}", database, editor);
                },
                CfgAction::Set { database, editor } => {
                    match &*database {
                        Some(p) => {
                            println!("Set db path to: {:?}", p);
                        },
                        None => {
                            println!("Set to original default task db file");
                        }
                    }

                    match &*editor {
                        Some(p) => {
                            println!("Set editor path to: {:?}", p);
                        }
                        None => {
                            println!("Set to original default editor path");
                        }
                    }
                }
            };
        },

        Action::Move { src_id, dest_id } => {
            println!("Move {} to {}", src_id, dest_id);
            if src_id != dest_id {
                tm.move_task(*src_id, *dest_id);
            } else {
                println!("Well played! だが断る！");
            }
        },

        Action::Remove { ids, force } => {
            if *force {
                println!("Forcefully remove {}", ids);
                tm.remove_tasks(ids.to_string());
            } else {
                println!("Remove {}", ids);
                tm.remove_tasks(ids.to_string());
            }
        },

        Action::Show { ids, full } => {
            if *full {
                tm.show_active_tasks_in_full();
            } else {
                println!("Should show {:?} in brief (default)", ids);
                tm.show_active_tasks();
            }
        },

        Action::Star { ids } => {
            println!("Star {}", ids);
        }
    };
}
