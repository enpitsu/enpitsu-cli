extern crate json;

use std::fs;

pub fn read_file_to_json(file_loc: &str) -> json::Result<json::JsonValue> {
    let contents: String = fs::read_to_string(file_loc)
        .expect("Could not read file");

    json::parse(&contents)
}
