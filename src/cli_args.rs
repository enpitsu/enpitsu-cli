use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(
    rename_all = "kebab-case"
)]
pub struct CliArgs {
    #[structopt(subcommand)]
    pub action: Action
}

#[derive(StructOpt, Debug)]
#[structopt(
    rename_all = "kebab-case",
)]
pub enum Action {
    #[structopt(about = "Add new task")]
    Add {
        #[structopt(help = "Title of the task", required = true)]
        title: String
    },
    #[structopt(about = "Get/set config")]
    Config {
        #[structopt(subcommand)]
        cfgaction: CfgAction
    },
    #[structopt(about = "Move task to id")]
    Move {
        #[structopt(help = "Source task", required = true)]
        src_id: usize,
        #[structopt(help = "Destination id", required = true)]
        dest_id: usize
    },
    #[structopt(about = "Remove task(s)")]
    Remove {
        /// List of tasks to remove [Format: a,b,c-d]
        /// 
        /// Examples:
        ///
        /// enpitsu remove 1,3,5-7
        /// Result: Tasks 1, 3, 5, 6, 7 are removed
        #[structopt(required = true)]
        ids: String,

        #[structopt(long, short, help = "Force removal without confirmation")]
        force: bool
    },
    #[structopt(about = "Show task(s)")]
    Show {
        #[structopt(help = "List of tasks to show [Format: a,b,c-d]")]
        ids: Option<String>,
        #[structopt(long, short, help = "Show full information on task(s)")]
        full: bool
    },
    #[structopt(about = "Star task(s)")]
    Star {
        /// List of tasks to star [Format: a,b,c-d]
        ///
        /// Examples:
        ///
        /// enpitsu star 1,3,5-7
        /// Result: Tasks 1, 3, 5, 6, 7 are starred
        #[structopt(required = true)]
        ids: String
    }
}

#[derive(StructOpt, Debug)]
#[structopt(
    rename_all = "kebab-case"
)]
pub enum CfgAction {
    #[structopt(about = "Get config")]
    Get {
        /// Path to current default task database
        #[structopt(short)]
        database: bool,
        /// Path to current default editor
        #[structopt(short)]
        editor: bool
    },
    #[structopt(about = "Set config")]
    Set {
        /// Set path to default task database file. Defaults to $XDG_DATA_HOME/enpitsu/tasks.db
        #[structopt(long, short)]
        database: Option<PathBuf>,
        /// Set path to default editor for editing notes. Defaults to /usr/bin/nano
        #[structopt(long, short)]
        editor: Option<PathBuf>
    }
}
